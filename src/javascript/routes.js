import AppHome from '@/components/AppHome';
import AppWizard from '@/components/AppWizard';


const routes = [
    {
        path: '/',
        name: 'Home',
        component: AppHome
    },
    {
        path: '/wizard',
        name: 'Wizard',
        component: AppWizard
    }
];

export default routes;
