//jquery
import $ from 'jquery';
window.jQuery = $;
window.$ = $;
//popper
import Popper from 'popper.js/dist/umd/popper.js';
window.Popper = Popper;
// Import Vue
import Vue from 'vue';
import VueRouter from 'vue-router';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
// Import Vue App, routes, store
import App from './App';
import routes from './javascript/routes';

Vue.use(VueRouter);
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@fortawesome/fontawesome-free'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch, faBell, faCalendarAlt, faUserSecret, faChartLine, faClone, faMagic, faStickyNote, faChartPie, faCertificate, faCaretRight, faCaretDown, faBullseye, faLayerGroup, faRocket} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add( faSearch, faBell, faCalendarAlt, faUserSecret, faChartLine, faClone, faStickyNote, faMagic, faChartPie, faCertificate, faCaretRight, faCaretDown, faBullseye, faLayerGroup, faRocket)

Vue.component('font-awesome-icon', FontAwesomeIcon)
import 'vue-bootstrap-selectpicker/dist/css/vue-bootstrap-selectpicker.min.css'
import SelectPicker from 'vue-bootstrap-selectpicker'

Vue.use(SelectPicker)
Vue.config.productionTip = false

import CountryFlag from 'vue-country-flag'

Vue.component('country-flag', CountryFlag)

import vSelect from 'vue-select'

Vue.component('v-select', vSelect)
import 'vue-select/dist/vue-select.css';

import { BreadcrumbPlugin } from 'bootstrap-vue'
Vue.use(BreadcrumbPlugin)

import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
Vue.use(VueFormWizard)
import VueFormGenerator from 'vue-form-generator'
import 'vue-form-generator/dist/vfg.css'

Vue.use(VueFormGenerator)
//js
import './javascript/app';
//sass
import './sass/style.sass';
// Configure router
const router = new VueRouter({
    routes,
    linkActiveClass: 'active',
    mode: 'history'
});

new Vue({
    el: '#app',
    render: h => h(App),
    router
});
